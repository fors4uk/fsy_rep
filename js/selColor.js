$(window).load(function () {
	$('#before-load').find('i').fadeOut(5500).end().delay(3000).fadeOut('slow');
	i = 0;
	setInterval(function () {
		if (i <= 100) {
			$('#proloadText').text(i + '%');
			i++;
		}
	}, 27);
});

$(document).ready(function () {
	$('#yourPhone').on('click', yourPhone);
	$('.shampoo1').on('click', shampoo1);
	$('.shampoobalz').on('click', shampoobalz);
	$('.balz1').on('click', balz1);
	$('#yourEmail').on('click', yourEmail);
	$('#whatProblem').on('click', selectProblem);
	$('.selBlue').on('click', selBlue);
	$('.selWhite').on('click', selWhite);
	$('.selYellow').on('click', selYellow);
	$('.selRed').on('click', selRed);
	$('.selGreen').on('click', selGreen);
	$('.selBlue1').on('click', selBlue);
	$('.selWhite1').on('click', selWhite);
	$('.selYellow1').on('click', selYellow);
	$('.selRed1').on('click', selRed);
	$('.selGreen1').on('click', selGreen);
	$('.selBlue2').on('click', selBlue2);
	$('.selWhite2').on('click', selWhite2);
	$('.selYellow2').on('click', selYellow2);
	$('.selRed2').on('click', selRed2);
	$('.selGreen2').on('click', selGreen2);
	$('#yourName3').on('click', function () {
		clearInterval(intervalID);
	});
	$('#yourName3').on('click', yourName1);
	$('#selectArom').on('mouseout', selectArom);
	$('#selectArom1').on('mouseout', selectArom1);
	$('.bottle').on('mouseover', btlparalax);
	var intervalID = setInterval(function () {
		var value = $('#yourName3').val();
		$('#yourName1').val(value);
		$('.yourName').text(value);
		$('.yourName1').text(value);
	}, 100);
	$('a[href*=#]').bind("click", function (e) {
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top
		}, 1000);
		e.preventDefault();
	});
	return false;
});


setInterval(function () {
	if ($('#checkbox-id1').prop('checked')) {
		var a1 = '1';
	} else {
		var a1 = '';
	}
	if ($('#checkbox-id2').prop('checked')) {
		var a2 = '2';
	} else {
		var a2 = '';
	}
	if ($('#checkbox-id3').prop('checked')) {
		var a3 = '3';
	} else {
		var a3 = '';
	}
	if ($('#checkbox-id4').prop('checked')) {
		var a4 = '4';
	} else {
		var a4 = '';
	}
	if ($('#checkbox-id5').prop('checked')) {
		var a5 = '5';
	} else {
		var a5 = '';
	}
	if ($('#checkbox-id6').prop('checked')) {
		var a6 = '6';
	} else {
		var a6 = '';
	}
	if ($('#checkbox-id7').prop('checked')) {
		var a7 = '7';
	} else {
		var a7 = '';
	}
	if ($('#checkbox-id8').prop('checked')) {
		var a8 = '8';
	} else {
		var a8 = '';
	}
	$('#whatProblem').val(a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8);
	var vall = $('#whatProblem').val();

	if (vall.length > 0) {
		$('.nameWhatProblem').text('Что беспокит / Какая проблематика волос');
	} else {
		$('.nameWhatProblem').text('');
	}
	var spl = vall.split('');
	$('#whatProblem').val(spl.join(', '));
}, 100);

var yes = true;

function yourPhone() {
	if (yes) {
		$('.phone1').append('<b class="phone">Номер телефона</b><br>');
		$('#yourPhone').val('+380');
		yes = false;
	}
}

var yes1 = true;

function yourEmail() {
	setInterval(function () {
		if ($('#yourEmail').val().length >= 1) {
			if (yes1) {
				$('.email1').append('<b class="email">E-mail</b><br>');
				yes1 = false;
			}
		}
	}, 100);
}

var click = true;

function shampoo1() {
	$('.footerForm1').removeClass('footerForm2');
	$('.yourName2Ob').removeClass('yourName30b');
	$('.yourName2Ob').removeClass('yourName40b');
	$('.yourName2Ob').addClass('yourName40b');
	$('#nameColor').text('Цвет шампуня');
	$('#nameArom').text('Аромат шампуня');
	$('#addBalz').fadeOut();
	click = true;
	$('.balm1').removeClass('balmblue');
	$('.balm1').removeClass('balmpink');
	$('.balm1').removeClass('balmyellow');
	$('.balm1').removeClass('balmred');
	$('.balm1').removeClass('balmgreen');
	$('.balm1').removeClass('shampooblue');
	$('.balm1').removeClass('shampoopink');
	$('.balm1').removeClass('shampooyellow');
	$('.balm1').removeClass('shampoored');
	$('.balm1').removeClass('shampoogreen');
	$('.balm2').removeClass('balmblue');
	$('.balm2').removeClass('balmpink');
	$('.balm2').removeClass('balmyellow');
	$('.balm2').removeClass('balmred');
	$('.balm2').removeClass('balmgreen');
	$('.balm2').removeClass('shampooblue');
	$('.balm2').removeClass('shampoopink');
	$('.balm2').removeClass('shampooyellow');
	$('.balm2').removeClass('shampoored');
	$('.balm2').removeClass('shampoogreen');
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var color = $('#yellow').val();
	$('.balm').addClass('shampoo' + color);
	$('.selShadow1').removeClass('shadow1');
	$('.selShadow2').removeClass('shadow2');
	$('.selShadow3').removeClass('shadow3');
	$('.selActive1').removeClass('activeCol4');
	$('.selActive2').removeClass('activeCol2');
	$('.selActive3').removeClass('activeCol3');
	$('.selShadow1').addClass('shadow1');
	$('.selActive1').addClass('activeCol4');
	$('#selectPos').val('shampoo');
}

function balz1() {
	$('.footerForm1').removeClass('footerForm2');
	$('.yourName2Ob').removeClass('yourName30b');
	$('.yourName2Ob').removeClass('yourName40b');
	$('#nameColor').text('Цвет бальзама');
	$('#nameArom').text('Аромат бальзама');
	$('#addBalz').fadeOut();
	click = true;
	$('.balm1').removeClass('balmblue');
	$('.balm1').removeClass('balmpink');
	$('.balm1').removeClass('balmyellow');
	$('.balm1').removeClass('balmred');
	$('.balm1').removeClass('balmgreen');
	$('.balm1').removeClass('shampooblue');
	$('.balm1').removeClass('shampoopink');
	$('.balm1').removeClass('shampooyellow');
	$('.balm1').removeClass('shampoored');
	$('.balm1').removeClass('shampoogreen');
	$('.balm2').removeClass('balmblue');
	$('.balm2').removeClass('balmpink');
	$('.balm2').removeClass('balmyellow');
	$('.balm2').removeClass('balmred');
	$('.balm2').removeClass('balmgreen');
	$('.balm2').removeClass('shampooblue');
	$('.balm2').removeClass('shampoopink');
	$('.balm2').removeClass('shampooyellow');
	$('.balm2').removeClass('shampoored');
	$('.balm2').removeClass('shampoogreen');
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var color = $('#yellow').val();
	$('.balm').addClass('balm' + color);
	$('.selShadow1').removeClass('shadow1');
	$('.selShadow2').removeClass('shadow2');
	$('.selShadow3').removeClass('shadow3');
	$('.selActive1').removeClass('activeCol4');
	$('.selActive2').removeClass('activeCol2');
	$('.selActive3').removeClass('activeCol3');
	$('.selShadow2').addClass('shadow2');
	$('.selActive2').addClass('activeCol2');
	$('#selectPos').val('balm');
}

function shampoobalz() {
	$('.footerForm1').addClass('footerForm2');
	$('.yourName2Ob').removeClass('yourName30b');
	$('.yourName2Ob').removeClass('yourName40b');
	$('.yourName2Ob').addClass('yourName30b');
	$('#nameColor').text('Цвет шампуня');
	$('#nameArom').text('Аромат шампуня');
	if (click) {
		$('#addBalz').fadeIn();
		click = false;
	}
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	$('.balm1').addClass('shampooyellow');
	$('.balm2').addClass('balmyellow');
	$('.selShadow1').removeClass('shadow1');
	$('.selShadow2').removeClass('shadow2');
	$('.selShadow3').removeClass('shadow3');
	$('.selActive1').removeClass('activeCol4');
	$('.selActive2').removeClass('activeCol2');
	$('.selActive3').removeClass('activeCol3');
	$('.selShadow3').addClass('shadow3');
	$('.selActive3').addClass('activeCol3');
	$('#selectPos').val('shampooAndBalz');
}


function selBlue() {
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var product = $('#selectPos').val();
	$('.balm').addClass(product + 'blue');
	$('.selBlue').removeClass('activeCol');
	$('.selWhite').removeClass('activeCol');
	$('.selYellow').removeClass('activeCol');
	$('.selRed').removeClass('activeCol');
	$('.selGreen').removeClass('activeCol');
	$('.selBlue1').removeClass('activeCol1');
	$('.selWhite1').removeClass('activeCol1');
	$('.selYellow1').removeClass('activeCol1');
	$('.selRed1').removeClass('activeCol1');
	$('.selGreen1').removeClass('activeCol1');
	$('.selBlue').addClass('activeCol');
	$('.selBlue1').addClass('activeCol1');
	$('#yellow').val('blue');
	$('#arom1').removeAttr('style');
	$('#arom2').removeAttr('style');
	$('#arom3').removeAttr('style');
	$('#arom4').removeAttr('style');
	$('#arom1').attr('style', 'background-color: #e9f7ff;');
	$('#arom2').attr('style', 'background-color: #e9f7ff;');
	$('#arom3').attr('style', 'background-color: #e9f7ff;');
	$('#arom4').attr('style', 'background-color: #e9f7ff;');
	$('.yourColor').removeClass('whiteBg');
	$('.yourColor').removeClass('blueBg');
	$('.yourColor').removeClass('yellowBg');
	$('.yourColor').removeClass('redBg');
	$('.yourColor').removeClass('greenBg');
	$('.splashCol').removeClass('splashWhite');
	$('.splashCol').removeClass('splashBlue');
	$('.splashCol').removeClass('splashYellow');
	$('.splashCol').removeClass('splashRed');
	$('.splashCol').removeClass('splashGreen');
	$('.footerForm').removeClass('bgWhite');
	$('.footerForm').removeClass('bgBlue');
	$('.footerForm').removeClass('bgYellow');
	$('.footerForm').removeClass('bgRed');
	$('.footerForm').removeClass('bgGreen');
	$('.yourColor').addClass('blueBg');
	$('.splashCol').addClass('splashBlue');
	$('.footerForm').addClass('bgBlue');
}

function selWhite() {
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var product = $('#selectPos').val();
	$('.balm').addClass(product + 'pink');
	$('.selBlue').removeClass('activeCol');
	$('.selWhite').removeClass('activeCol');
	$('.selYellow').removeClass('activeCol');
	$('.selRed').removeClass('activeCol');
	$('.selGreen').removeClass('activeCol');
	$('.selBlue1').removeClass('activeCol1');
	$('.selWhite1').removeClass('activeCol1');
	$('.selYellow1').removeClass('activeCol1');
	$('.selRed1').removeClass('activeCol1');
	$('.selGreen1').removeClass('activeCol1');
	$('.selWhite').addClass('activeCol');
	$('.selWhite1').addClass('activeCol1');
	$('#yellow').val('pink');
	$('#arom1').removeAttr('style');
	$('#arom2').removeAttr('style');
	$('#arom3').removeAttr('style');
	$('#arom4').removeAttr('style');
	$('#arom1').attr('style', 'background-color: #fbf4f4;');
	$('#arom2').attr('style', 'background-color: #fbf4f4;');
	$('#arom3').attr('style', 'background-color: #fbf4f4;');
	$('#arom4').attr('style', 'background-color: #fbf4f4;');
	$('.yourColor').removeClass('whiteBg');
	$('.yourColor').removeClass('blueBg');
	$('.yourColor').removeClass('yellowBg');
	$('.yourColor').removeClass('redBg');
	$('.yourColor').removeClass('greenBg');
	$('.splashCol').removeClass('splashWhite');
	$('.splashCol').removeClass('splashBlue');
	$('.splashCol').removeClass('splashYellow');
	$('.splashCol').removeClass('splashRed');
	$('.splashCol').removeClass('splashGreen');
	$('.footerForm').removeClass('bgWhite');
	$('.footerForm').removeClass('bgBlue');
	$('.footerForm').removeClass('bgYellow');
	$('.footerForm').removeClass('bgRed');
	$('.footerForm').removeClass('bgGreen');
	$('.yourColor').addClass('whiteBg');
	$('.splashCol').addClass('splashWhite');
	$('.footerForm').addClass('bgWhite');
}

function selYellow() {
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var product = $('#selectPos').val();
	$('.balm').addClass(product + 'yellow');
	$('.selBlue').removeClass('activeCol');
	$('.selWhite').removeClass('activeCol');
	$('.selYellow').removeClass('activeCol');
	$('.selRed').removeClass('activeCol');
	$('.selGreen').removeClass('activeCol');
	$('.selBlue1').removeClass('activeCol1');
	$('.selWhite1').removeClass('activeCol1');
	$('.selYellow1').removeClass('activeCol1');
	$('.selRed1').removeClass('activeCol1');
	$('.selGreen1').removeClass('activeCol1');
	$('.selYellow').addClass('activeCol');
	$('.selYellow1').addClass('activeCol1');
	$('#yellow').val('yellow');
	$('#arom1').removeAttr('style');
	$('#arom2').removeAttr('style');
	$('#arom3').removeAttr('style');
	$('#arom4').removeAttr('style');
	$('#arom1').attr('style', 'background-color: #fff6e8;');
	$('#arom2').attr('style', 'background-color: #fff6e8;');
	$('#arom3').attr('style', 'background-color: #fff6e8;');
	$('#arom4').attr('style', 'background-color: #fff6e8;');
	$('.yourColor').removeClass('whiteBg');
	$('.yourColor').removeClass('blueBg');
	$('.yourColor').removeClass('yellowBg');
	$('.yourColor').removeClass('redBg');
	$('.yourColor').removeClass('greenBg');
	$('.splashCol').removeClass('splashWhite');
	$('.splashCol').removeClass('splashBlue');
	$('.splashCol').removeClass('splashYellow');
	$('.splashCol').removeClass('splashRed');
	$('.splashCol').removeClass('splashGreen');
	$('.footerForm').removeClass('bgWhite');
	$('.footerForm').removeClass('bgBlue');
	$('.footerForm').removeClass('bgYellow');
	$('.footerForm').removeClass('bgRed');
	$('.footerForm').removeClass('bgGreen');
	$('.yourColor').addClass('yellowBg');
	$('.splashCol').addClass('splashYellow');
	$('.footerForm').addClass('bgYellow');
}

function selRed() {
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var product = $('#selectPos').val();
	$('.balm').addClass(product + 'red');
	$('.selBlue').removeClass('activeCol');
	$('.selWhite').removeClass('activeCol');
	$('.selYellow').removeClass('activeCol');
	$('.selRed').removeClass('activeCol');
	$('.selGreen').removeClass('activeCol');
	$('.selBlue1').removeClass('activeCol1');
	$('.selWhite1').removeClass('activeCol1');
	$('.selYellow1').removeClass('activeCol1');
	$('.selRed1').removeClass('activeCol1');
	$('.selGreen1').removeClass('activeCol1');
	$('.selRed').addClass('activeCol');
	$('.selRed1').addClass('activeCol1');
	$('#yellow').val('red');
	$('#arom1').removeAttr('style');
	$('#arom2').removeAttr('style');
	$('#arom3').removeAttr('style');
	$('#arom4').removeAttr('style');
	$('#arom1').attr('style', 'background-color: #ffdfdf;');
	$('#arom2').attr('style', 'background-color: #ffdfdf;');
	$('#arom3').attr('style', 'background-color: #ffdfdf;');
	$('#arom4').attr('style', 'background-color: #ffdfdf;');
	$('.yourColor').removeClass('whiteBg');
	$('.yourColor').removeClass('blueBg');
	$('.yourColor').removeClass('yellowBg');
	$('.yourColor').removeClass('redBg');
	$('.yourColor').removeClass('greenBg');
	$('.splashCol').removeClass('splashWhite');
	$('.splashCol').removeClass('splashBlue');
	$('.splashCol').removeClass('splashYellow');
	$('.splashCol').removeClass('splashRed');
	$('.splashCol').removeClass('splashGreen');
	$('.footerForm').removeClass('bgWhite');
	$('.footerForm').removeClass('bgBlue');
	$('.footerForm').removeClass('bgYellow');
	$('.footerForm').removeClass('bgRed');
	$('.footerForm').removeClass('bgGreen');
	$('.yourColor').addClass('redBg');
	$('.splashCol').addClass('splashRed');
	$('.footerForm').addClass('bgRed');
}

function selGreen() {
	$('.balm').removeClass('balmblue');
	$('.balm').removeClass('balmpink');
	$('.balm').removeClass('balmyellow');
	$('.balm').removeClass('balmred');
	$('.balm').removeClass('balmgreen');
	$('.balm').removeClass('shampooblue');
	$('.balm').removeClass('shampoopink');
	$('.balm').removeClass('shampooyellow');
	$('.balm').removeClass('shampoored');
	$('.balm').removeClass('shampoogreen');
	var product = $('#selectPos').val();
	$('.balm').addClass(product + 'green');
	$('.selBlue').removeClass('activeCol');
	$('.selWhite').removeClass('activeCol');
	$('.selYellow').removeClass('activeCol');
	$('.selRed').removeClass('activeCol');
	$('.selGreen').removeClass('activeCol');
	$('.selBlue1').removeClass('activeCol1');
	$('.selWhite1').removeClass('activeCol1');
	$('.selYellow1').removeClass('activeCol1');
	$('.selRed1').removeClass('activeCol1');
	$('.selGreen1').removeClass('activeCol1');
	$('.selGreen').addClass('activeCol');
	$('.selGreen1').addClass('activeCol1');
	$('#yellow').val('green');
	$('#arom1').removeAttr('style');
	$('#arom2').removeAttr('style');
	$('#arom3').removeAttr('style');
	$('#arom4').removeAttr('style');
	$('#arom1').attr('style', 'background-color: #ddfae8;');
	$('#arom2').attr('style', 'background-color: #ddfae8;');
	$('#arom3').attr('style', 'background-color: #ddfae8;');
	$('#arom4').attr('style', 'background-color: #ddfae8;');
	$('.yourColor').removeClass('whiteBg');
	$('.yourColor').removeClass('blueBg');
	$('.yourColor').removeClass('yellowBg');
	$('.yourColor').removeClass('redBg');
	$('.yourColor').removeClass('greenBg');
	$('.splashCol').removeClass('splashWhite');
	$('.splashCol').removeClass('splashBlue');
	$('.splashCol').removeClass('splashYellow');
	$('.splashCol').removeClass('splashRed');
	$('.splashCol').removeClass('splashGreen');
	$('.footerForm').removeClass('bgWhite');
	$('.footerForm').removeClass('bgBlue');
	$('.footerForm').removeClass('bgYellow');
	$('.footerForm').removeClass('bgRed');
	$('.footerForm').removeClass('bgGreen');
	$('.yourColor').addClass('greenBg');
	$('.splashCol').addClass('splashGreen');
	$('.footerForm').addClass('bgGreen');
}

function selBlue2() {
	$('.selBlue2').removeClass('activeCol1');
	$('.selWhite2').removeClass('activeCol1');
	$('.selYellow2').removeClass('activeCol1');
	$('.selRed2').removeClass('activeCol1');
	$('.selGreen2').removeClass('activeCol1');
	$('.selBlue2').addClass('activeCol1');
	$('#yellow1').val('blue');
}

function selWhite2() {
	$('.selBlue2').removeClass('activeCol1');
	$('.selWhite2').removeClass('activeCol1');
	$('.selYellow2').removeClass('activeCol1');
	$('.selRed2').removeClass('activeCol1');
	$('.selGreen2').removeClass('activeCol1');
	$('.selWhite2').addClass('activeCol1');
	$('#yellow1').val('pink');
}

function selYellow2() {
	$('.selBlue2').removeClass('activeCol1');
	$('.selWhite2').removeClass('activeCol1');
	$('.selYellow2').removeClass('activeCol1');
	$('.selRed2').removeClass('activeCol1');
	$('.selGreen2').removeClass('activeCol1');
	$('.selYellow2').addClass('activeCol1');
	$('#yellow1').val('yellow');
}

function selRed2() {
	$('.selBlue2').removeClass('activeCol1');
	$('.selWhite2').removeClass('activeCol1');
	$('.selYellow2').removeClass('activeCol1');
	$('.selRed2').removeClass('activeCol1');
	$('.selGreen2').removeClass('activeCol1');
	$('.selRed2').addClass('activeCol1');
	$('#yellow1').val('red');
}

function selGreen2() {
	$('.selBlue2').removeClass('activeCol1');
	$('.selWhite2').removeClass('activeCol1');
	$('.selYellow2').removeClass('activeCol1');
	$('.selRed2').removeClass('activeCol1');
	$('.selGreen2').removeClass('activeCol1');
	$('.selGreen2').addClass('activeCol1');
	$('#yellow1').val('green');
}

function yourName1() {
	var intervalID = setInterval(function () {
		var value = $('#yourName3').val();
		$('.yourName').text(value);
		$('.yourName2').text(value);
	}, 100);
}

function selectArom() {
	var val = $('#selectArom').val();
	if (val == 'arom1') {
		$('.yourArom').removeClass('arom1a');
		$('.yourArom1').removeClass('arom1b');
		$('.yourArom').removeClass('arom2a');
		$('.yourArom1').removeClass('arom2b');
		$('.yourArom').removeClass('arom3a');
		$('.yourArom1').removeClass('arom3b');
		$('.yourArom').removeClass('arom4a');
		$('.yourArom1').removeClass('arom4b');
		$('.yourArom').addClass('arom1a');
		$('.yourArom1').addClass('arom1b');
	}
	if (val == 'arom2') {
		$('.yourArom').removeClass('arom1a');
		$('.yourArom1').removeClass('arom1b');
		$('.yourArom').removeClass('arom2a');
		$('.yourArom1').removeClass('arom2b');
		$('.yourArom').removeClass('arom3a');
		$('.yourArom1').removeClass('arom3b');
		$('.yourArom').removeClass('arom4a');
		$('.yourArom1').removeClass('arom4b');
		$('.yourArom').addClass('arom2a');
		$('.yourArom1').addClass('arom2b');
	}
	if (val == 'arom3') {
		$('.yourArom').removeClass('arom1a');
		$('.yourArom1').removeClass('arom1b');
		$('.yourArom').removeClass('arom2a');
		$('.yourArom1').removeClass('arom2b');
		$('.yourArom').removeClass('arom3a');
		$('.yourArom1').removeClass('arom3b');
		$('.yourArom').removeClass('arom4a');
		$('.yourArom1').removeClass('arom4b');
		$('.yourArom').addClass('arom3a');
		$('.yourArom1').addClass('arom3b');
	}
	if (val == 'arom4') {
		$('.yourArom').removeClass('arom1a');
		$('.yourArom1').removeClass('arom1b');
		$('.yourArom').removeClass('arom2a');
		$('.yourArom1').removeClass('arom2b');
		$('.yourArom').removeClass('arom3a');
		$('.yourArom1').removeClass('arom3b');
		$('.yourArom').removeClass('arom4a');
		$('.yourArom1').removeClass('arom4b');
		$('.yourArom').addClass('arom4a');
		$('.yourArom1').addClass('arom4b');
	}
}

function selectArom1() {
	var val = $('#selectArom1').val();
	if (val == 'arom1') {
		$('.bgArom').removeClass('bgarom1');
		$('.bgArom').removeClass('bgarom2');
		$('.bgArom').removeClass('bgarom3');
		$('.bgArom').removeClass('bgarom4');
		$('.bgArom').addClass('bgarom1');
	}
	if (val == 'arom2') {
		$('.bgArom').removeClass('bgarom1');
		$('.bgArom').removeClass('bgarom2');
		$('.bgArom').removeClass('bgarom3');
		$('.bgArom').removeClass('bgarom4');
		$('.bgArom').addClass('bgarom2');
	}
	if (val == 'arom3') {
		$('.bgArom').removeClass('bgarom1');
		$('.bgArom').removeClass('bgarom2');
		$('.bgArom').removeClass('bgarom3');
		$('.bgArom').removeClass('bgarom4');
		$('.bgArom').addClass('bgarom3');
	}
	if (val == 'arom4') {
		$('.bgArom').removeClass('bgarom1');
		$('.bgArom').removeClass('bgarom2');
		$('.bgArom').removeClass('bgarom3');
		$('.bgArom').removeClass('bgarom4');
		$('.bgArom').addClass('bgarom4');
	}
}

function btlparalax() {
	var $layer_0 = $('.layer-0'),
		$layer_1 = $('.layer-1'),
		$layer_2 = $('.layer-2'),
		$layer_3 = $('.layer-3'),
		$x_axis = $('#x-axis'),
		$y_axis = $('#y-axis'),
		$container = $('body'),
		container_w = $container.width(),
		container_h = $container.height();

	$('.present').on('mousemove.parallax', function (event) {
		var pos_x = event.pageX,
			pos_y = event.pageY,
			left = 0,
			top = 0;

		left = container_w / 2 - pos_x;
		top = container_h / 2 - pos_y;

		TweenMax.to(
			$x_axis,
			1, {
				css: {
					transform: 'translateX(' + (left * -1) + 'px)'
				},
				ease: Expo.easeOut,
				overwrite: 'all'
			});

		TweenMax.to(
			$y_axis,
			1, {
				css: {
					transform: 'translateY(' + (top * -1) + 'px)'
				},
				ease: Expo.easeOut,
				overwrite: 'all'
			});

		TweenMax.to(
			$layer_3,
			1, {
				css: {
					transform: 'translateX(' + left / 12 + 'px) translateY(' + top / 10 + 'px)'
				},
				ease: Expo.easeOut,
				overwrite: 'all'
			});

		TweenMax.to(
			$layer_2,
			1, {
				css: {
					transform: 'translateX(' + left / 12 + 'px) translateY(' + top / 5 + 'px)'
				},
				ease: Expo.easeOut,
				overwrite: 'all'
			});

		TweenMax.to(
			$layer_1,
			1, {
				css: {
					transform: 'translateX(' + left / 12 + 'px) translateY(' + top / 9 + 'px)'
				},
				ease: Expo.easeOut,
				overwrite: 'all'
			});

		TweenMax.to(
			$layer_0,
			10, {
				css: {
					transform: 'rotate(' + left / 200 + 'deg)'
				},
				ease: Expo.easeOut,
				overwrite: 'none'
			}
		)
	});
}
var i1 = 0;
var isEvent = false;

function selectProblem() {
	if (!isEvent) {
		isEvent = true;
		setTimeout(function () {
			isEvent = false;
		}, 100);
		if (i1 % 2 == 0) {
			$('.selectProblem').fadeIn(100);
			i1++;
		} else {
			$('.selectProblem').fadeOut(100);
			i1++;
		}
	}
}
